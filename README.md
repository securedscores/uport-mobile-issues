# uPort Mobile Issue Tracker

This repository is a place to report and track issues encountered while using the uPort mobile app. File an issue [here](https://github.com/uport-project/uport-mobile-issues/issues) and we'll provide status updates as we address.

If you have general questions on how to use uPort, head to our [Gitter channel](https://gitter.im/uport-project/Lobby) for support.

For guides and information on how to get started integrating uPort with your application, check out our [developer documentation](http://developer.uport.me/).

For technical specifications on how our platform is designed, head to the [uPort Spec repo](https://github.com/uport-project/specs).

For issues or contributions to our uPort Connect library, head to its repository [here](https://github.com/uport-project/uport-connect).
